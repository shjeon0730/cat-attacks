import { useContext } from 'react';
import { GameControlContext } from '../Contexts/GameControlContext';
import './Entry.css';

export const Entry = () => {
    const { setGameStart } = useContext(GameControlContext);
    return (
        <>
            <h1>Cat Attacks!</h1>

            <div className='card'>
                <button onClick={() => setGameStart(true)}>Start Game!</button>
            </div>
        </>
    );
};
