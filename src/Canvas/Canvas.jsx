import './Canvas.css';

export const Canvas = props => {
    return (
        <canvas
            id='canvas'
            className='canvas'
            width='100'
            height='100'
        ></canvas>
    );
};
