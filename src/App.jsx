import React, { useState } from 'react';
import { Canvas } from './Canvas/Canvas';
import { Entry } from './Entry/Entry';
import './App.css';
import { GameControlContext } from './Contexts/GameControlContext';

function App() {
    const [isGameStarted, setGameStart] = useState(false);
    const content = isGameStarted ? <Canvas /> : <Entry />;
    return (
        <div className='App'>
            <GameControlContext.Provider
                value={{ isGameStarted, setGameStart }}
            >
                {content}
            </GameControlContext.Provider>
        </div>
    );
}

export default App;
