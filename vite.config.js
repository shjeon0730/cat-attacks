import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/

export default defineConfig(({ command, mode }) => {
    console.log('hey ', mode);
    if (command == 'serve') {
        console.log('dev-mode');
        return {
            plugins: [react()],
        };
    } else {
        //build
        console.log('build mode');
        return {
            plugins: [react()],
        };
    }
});
